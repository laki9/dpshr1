<?php
namespace DepositShop;

use Curl\Curl;

class Parser
{
    private $login = '';
    private $password = '';
    public $debug = false;
    public $cookie_file = 'cookies.txt';
    public $user_agent = 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.66 Safari/537.36';
    public $files_directory = 'files';
    private $curl;
    
    public function __construct()
    {
        if ($this->debug) {
            echo "Parser loaded!\n";
        }
        $this->curl = new Curl();
        $this->curl->setCookieFile($this->cookie_file);
        $this->curl->setCookieJar($this->cookie_file);
        $this->curl->setHeader('User-Agent', $this->user_agent);
        if (!file_exists($this->files_directory)) {
            mkdir($this->files_directory);
        }
    }
    
    public function setLoginData($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }
    
    public function login()
    {
        $this->curl->get('https://ru.depositphotos.com/login.html', array(
            'callback' => 'depositLogin_' . md5(time()),
            'username' => $this->login,
            'password' => $this->password,
            'dataType' => 'jsonp',
            '_' => time(),
        ));
    }
    
    public function getFreeFiles()
    {
        $free_files_urls = $this->getFreeFilesUrls();
        foreach ($free_files_urls as $file => $url) {
            $file_sizes = $this->getFileSizes($url, $file_id);
            if (!$file_id) {
                continue;
            }
            foreach ($file_sizes as $file_size) {
                $this->downloadFileSize($file, $file_id, $file_size);
            }
        }
    }
    
    public function getFreeFilesUrls()
    {
        $this->curl->get('http://ru.depositphotos.com/free-files.html');
        $this->setCurlHtml();
        $d = new \DOMDocument();
        @$d->loadHTML($this->curl->response);
        $xpath = new \DOMXPath($d);
        $meta_data = $xpath->query("//div[contains(@class, 'deposit-item')]/a[@class='item-holder']");
        $arr_files = array();
        foreach ($meta_data as $meta) {
            $meta_url = $meta->getAttribute('href');
            if (!$meta_url) {
                continue;
            }
            $file_name = array_pop(explode("/", $meta_url));
            $file_name = preg_replace("/^(.+)\.html$/", "\$1", $file_name);
            $file_name = preg_replace("/[^0-9a-z\-]/", "_", $file_name);
            $arr_files[$file_name] = $meta_url;
        }
        return $arr_files;
    }
    
    public function getFileSizes($url, &$fileId)
    {
        $fileId = preg_replace("/^.+\/ru\.depositphotos\.com\/([0-9]+)\/.+$/", "\$1", $url);
        $this->log("file id " . $fileId);
        $this->log("Parsing file " . $url);
        $this->setCurlHtml();
        $this->curl->get($url);
        $d = new \DOMDocument();
        @$d->loadHTML($this->curl->response);
        $xpath = new \DOMXPath($d);
        $file_sizes_dom = $xpath->query("//div[contains(@class, 'table-tr')]//input[contains(@name, 'size')]");
        $file_sizes = array();
        foreach ($file_sizes_dom as $file_size_el) {
            $file_size = $file_size_el->getAttribute('value');
            if ($xpath->query("//div[@data-size='" . $file_size . "']//span[@class='text-red']")->length or $xpath->query("//a[@data-size='" . $file_size . "' and @class='item-purchased']")->length) {
                $file_sizes[] = $file_size;
            }
        }
        return $file_sizes;
    }
    
    private function setCurlHtml()
    {
        $this->curl->setHeader('X-Requested-With', '');
        $this->curl->setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
    }
    
    private function setCurlJson()
    {
        $this->curl->setHeader('X-Requested-With', 'XMLHttpRequest');
        $this->curl->setHeader('Accept', 'application/json, */*; q=0.01');
    }
    
    public function downloadFileSize($file, $fileId, $fileSize)
    {
        $this->log("downloading $file file size $fileSize");
        $this->setCurlJson();
        $this->curl->post('http://ru.depositphotos.com/cart/purchase.html', array(
            'method' => 'credits',
            'id' => $fileId,
            'source' => 'view_item',
            'license' => 'standard',
            'subscription' => 0,
            'purchased' => 'false',
            'size' => $fileSize
        ));
        $this->log("download link " . $this->curl->response->data->link);
        if (!$this->checkFileSizeIntegrity($this->curl->response->data->link, $file_name)) {
            $this->log("let's download this file");
            $this->curl->download($this->curl->response->data->link, $file_name);
        } else {
            $this->log("file is ok, nothing to do");
        }
    }
    
    public function checkFileSizeIntegrity($downloadLink, &$fileName)
    {
        $headers = get_headers($downloadLink, 1);
        if (!isset($headers['Content-Disposition'])) {
            $this->log("problem with downloading file");
            return true;
        }
        $server_file_name = preg_replace('/.+filename="([^"]+)"/', '$1', $headers['Content-Disposition']);
        $fileName = preg_replace('/[^0-9a-z\.]/i', '_', $server_file_name);
        $fileName = $this->files_directory . DIRECTORY_SEPARATOR . $fileName;
        $this->log("server name {$server_file_name}, logical name {$fileName}");
        if (file_exists($fileName) and (filesize($fileName) == $headers['Content-Length'])) {
            return true;
        } else {
            return false;
        }
    }
    
    public function log($msg)
    {
        if ($this->debug) {
            echo "$msg\n";
        }
    }
}
