stable

Пример использования:

require_once 'vendor/autoload.php';

use DepositShop\Parser;

$parser = new Parser();
$parser->debug = true;
$parser->setLoginData('laki9' , 'loa123');
$parser->login();
//Пример загрузки всех файлов
$parser->getFreeFiles();
//Загрузка конкретного размера определенного файла 
$parser->downloadFileSize('stock-video-super-blood-moon-eclipse-timelapse', '85195140', 'smallweb');
